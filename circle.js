class Circle{
      constructor(radius, color){
            this.radius = radius;
            this.color = color;
            
        }

        area(){
           // let oppervlak = this.radius* this.radius * Math.PI;
            return this.radius* this.radius * Math.PI;  
        }

}

let red_circle= new Circle("5.6", "red" );
let green_circle = new Circle("2.3", "green");

console.log("Red circle area",red_circle.area());
console.log("Green circle area", green_circle.area());