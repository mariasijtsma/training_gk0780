document.addEventListener("DOMContentLoaded", onPageLoad);

function createParticipants(){

    let participantArray = new Array();
     let id = 101;
     let ellen = new Participant(id,"silver", "Ellen", "Elsinga");
     participantArray.push(ellen);
     id += 1;
     let bob = new Participant(id,"bronze", "Bob", "Bouwer" )
     id += 1;
     let maria = new Participant(id,"gold", "Maria", "Sijtsma" )
     participantArray.push(bob);
     participantArray.push(maria);
     
     return participantArray

     

     //ellen.greet();
     //bob.greet();
     //plaats de participanten in een lijst (Array)
     //Return Array
}



function letThemGreet(memberArray){
    memberArray.forEach(function(participant, index, array){
           participant.greet();

    });
}

 function appendParticipantToDOMElement(participant,element_id) {
       let parentElement = document.getElementById(element_id);
       let childElement = document.createElement('li');
       childElement.innerHTML = participant.firstName + " " + participant.lastName;
       parentElement.append(childElement); //hang in de DOM    
}

function showAsHtml(memberArray){
    appendParticipantToDOMElement(memberArray[0],'participantList');
    // vindt in het DOM het element waar je de lijst aan wilt toevoegen
    // plaats stuk voor stuk de participanten (toevoegen) aan de DOM


    /*
    for(let index in memberArray){
        let participant = memberArray[index];
        participant.greet();
    }
    */
}

// begin van de code uitvoering
function onPageLoad(){
   let participants = createParticipants();
   letThemGreet(participants);
   showAsHtml(participants);
}
